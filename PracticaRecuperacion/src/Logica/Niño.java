/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author peta_zetas
 */
public class Niño extends Thread{
    private Semaphore turnoTobogan, turnoTiovivo, turnoColumpio;
    private int nombre, edad;
    private ListaThreads colaTiovivo, colaTobogan, colaColumpio;
    private String lugarDeJuego = "Buscando atracción...";
    private Parque parque;

    public void setLugarDeJuego(String nombreJuego){
        lugarDeJuego = nombreJuego;
    }
    
    public String getLugarDeJuego(){
        return lugarDeJuego;
    }
    
    public Niño(int nombre, Parque parque,Semaphore turnoTobogan,Semaphore turnoTiovivo) {
        super.setName("Niño-" + nombre);
        this.edad = 2 + (nombre % 10);
        this.turnoTobogan = turnoTobogan;
        this.parque = parque;
        this.turnoTiovivo=turnoTiovivo;
    }
    

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
        
    
    
    public void salir(){
        System.out.println("Niño " + nombre + "ha salido del parque");
    }
    
    
    public void run(){
        try {
            Thread.sleep(200);
        } catch (InterruptedException ex) {
            Logger.getLogger(Niño.class.getName()).log(Level.SEVERE, null, ex);
        }
            
            parque.entrar(this);                                 //Selecciona atraccion y despues otra vez 
            parque.seleccionarAtraccion(this);
            
        
    }

    public Semaphore getTurnoTiovivo() {
        return turnoTiovivo;
    }

    public void setTurnoTiovivo(Semaphore turnoTiovivo) {
        this.turnoTiovivo = turnoTiovivo;
    }

    public Semaphore getTurnoColumpio() {
        return turnoColumpio;
    }

    public void setTurnoColumpio(Semaphore turnoColumpio) {
        this.turnoColumpio = turnoColumpio;
    }

    public Semaphore getTurnoTobogan() {
        return turnoTobogan;
    }

    public void setTurnoTobogan(Semaphore turnoTobogan) {
        this.turnoTobogan = turnoTobogan;
    }

    public int getNombre() {
        return nombre;
    }

    public void setNombre(int nombre) {
        this.nombre = nombre;
    }

    public ListaThreads getColaTiovivo() {
        return colaTiovivo;
    }

    public void setColaTiovivo(ListaThreads colaTiovivo) {
        this.colaTiovivo = colaTiovivo;
    }

    public ListaThreads getColaTobogan() {
        return colaTobogan;
    }

    public void setColaTobogan(ListaThreads colaTobogan) {
        this.colaTobogan = colaTobogan;
    }

    public ListaThreads getColaColumpio() {
        return colaColumpio;
    }

    public void setColaColumpio(ListaThreads colaColumpio) {
        this.colaColumpio = colaColumpio;
    }

    public Parque getParque() {
        return parque;
    }

    public void setParque(Parque parque) {
        this.parque = parque;
    }
      
}
