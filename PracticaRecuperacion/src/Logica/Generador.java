/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author roberto y samuel
 */
public class Generador extends Thread {
    private int total;
    private Parque parque;
    private Semaphore turnoT,turnoT2;  

    public Generador(int total, Parque parque, Semaphore turnoT,Semaphore turnoT2) {
        this.total = total;
        this.turnoT=turnoT;
        this.parque = parque;
        this.turnoT2=turnoT2;
    }
    
    public void run() {
        
        Niño niño;
        
        for(int i=1;i<=total;i++){
            parque.monitorPararParque();
            try {
                Thread.sleep(200);
            } catch (InterruptedException ex) {
                Logger.getLogger(Generador.class.getName()).log(Level.SEVERE, null, ex);
            }
            niño = new Niño(i, parque,turnoT,turnoT2);
            niño.start();
        }
    }
    
}
