/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;

/**
 *
 * @author roberto
 */
public class Parque {
    
    private CyclicBarrier barreraTobogan, barreraTiovivo, barreraColumpio1,barreraColumpio2, barreraColumpio3;
    private ListaThreads colaTobogan, colaTiovivo, colaColumpio,colaFuera, dentro, tobogan, tiovivo, columpio, colaColumpio1, colaColumpio2, colaColumpio3;
    private boolean detenido, libreColumpio1, libreColumpio2, libreColumpio3;
    private Semaphore turnoTobogan, turnoTiovivo, turnoColumpio;
    private JTextField jTextFieldColaColumpio,jTextFieldColumpio,jTextFieldDecidiendo,jTextFieldTobogan,jTextFieldColaTiovivo,jTextFieldTiovivo,jTextFieldColaTobogan,jTextFieldAños,jTextColumpio1, jTextColumpio2, jTextColumpio3 ;
   
    public Parque(JTextField jTextFieldColumpio,JTextField jTextFieldColaColumpio,JTextField jTextFieldTobogan, JTextField jTextFieldColaTobogan, JTextField jTextFieldTiovivo, JTextField jTextFieldColaTiovivo,JTextField jTextFieldDecidiendo,JTextField jTextFieldAños, JTextField jTextColumpio1,JTextField jTextColumpio2, JTextField jTextColumpio3 ){
        this.jTextFieldColumpio=jTextFieldColumpio;
        this.jTextFieldColaColumpio= jTextFieldColaColumpio;
        this.jTextFieldDecidiendo=jTextFieldDecidiendo;
        this.jTextFieldTobogan=jTextFieldTobogan;
        this.jTextFieldColaTiovivo=jTextFieldColaTiovivo;
        this.jTextFieldTiovivo=jTextFieldTiovivo;    
        this.jTextFieldColaTobogan=jTextFieldColaTobogan;
        this.jTextFieldAños=jTextFieldAños;
        this.jTextColumpio1 = jTextColumpio1;
        this.jTextColumpio2 = jTextColumpio2;
        this.jTextColumpio3 = jTextColumpio3;
        
        dentro=new ListaThreads();
        tobogan=new ListaThreads(jTextFieldTobogan);
        colaFuera=new ListaThreads(jTextFieldDecidiendo);
        colaTobogan=new ListaThreads(jTextFieldColaTobogan);
        colaTiovivo=new ListaThreads(jTextFieldColaTiovivo);
        colaColumpio=new ListaThreads(jTextFieldColaColumpio);
        colaColumpio1=new ListaThreads(jTextColumpio1);
        colaColumpio2=new ListaThreads(jTextColumpio2);
        colaColumpio3=new ListaThreads(jTextColumpio3);
        libreColumpio1 = true;
        libreColumpio2 = true;
        libreColumpio3 = true;

        tiovivo=new ListaThreads(jTextFieldTiovivo);
        barreraTobogan=new CyclicBarrier(2);
        barreraTiovivo=new CyclicBarrier(6);
        barreraColumpio1  = new CyclicBarrier(2);
        barreraColumpio2  = new CyclicBarrier(2);
        barreraColumpio3  = new CyclicBarrier(2);
        turnoTobogan=new Semaphore(1,true);
        turnoTiovivo = new Semaphore(5,true);
        turnoColumpio = new Semaphore(3,true);
          
    }
  
    public void seleccionarAtraccion(Niño niño){
        
        colaFuera.imprimir();
        monitorPararParque();
        int elegir=(2);
        
        if (elegir == 0){
           
            try {
                colaTiovivo.meter(niño);                //se mete niño en la cola
                System.out.println("Voy al tiovivo, soy "+niño.getName());
                niño.getTurnoTiovivo().acquire();
                tiovivo.meter(niño);
                colaTiovivo.sacar(niño);
                niño.sleep(5000);
                esAtendidoTiovivo();
                System.out.println("SALGO DEL TIOVIVO, soy "+niño.getId());
                
                niño.getTurnoTiovivo().release();
                
                tiovivo.sacar();
                dentro.sacar();
                colaFuera.meter(niño);
                reentrar(niño);
                
            } catch (InterruptedException ex) {
                Logger.getLogger(Parque.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println(ex);
            }
        }
        
        else if (elegir == 1){
            
            try{
                
                System.out.println("Voy al tobogan, soy "+niño.getName()+"tengo: "+niño.getEdad()+" años");
                colaTobogan.meter(niño);
                
                niño.getTurnoTobogan().acquire();
                
                jTextFieldAños.setText(String.valueOf(niño.getEdad())+" años");
                esAtendidoT();
                jTextFieldAños.setText(String.valueOf(niño.getEdad())+" años");
                jTextFieldAños.setText("");
                niño.getTurnoTobogan().release();
                
                dentro.sacar();
                colaFuera.meter(niño);
                reentrar(niño);
                
            }catch(Exception e){System.out.println(e);}
        }
        
        else if (elegir == 2){
            
            System.out.println("Voy a los columpios soy "+niño.getName());
            colaColumpio.meter(niño);
            columpiarse(niño);
            
        }
        colaFuera.sacar(niño);
        
    }
    /**
    * Código para columpio 
     * @param niño
    */
    
    public void columpiarse(Niño niño){
        
        monitorPararParque();
        
        try{
            turnoColumpio.acquire();
            if(libreColumpio1){
                
                libreColumpio1=false;
                barreraColumpio1.await();
                jTextColumpio1.setText(niño.getName());
                barreraColumpio1.await();
                jTextColumpio1.setText("");
                libreColumpio1=true;
                colaFuera.meter(niño);
                reentrar(niño);
              
            }
            
            else if(libreColumpio2){
                
                libreColumpio2=false;
                barreraColumpio2.await();
                jTextColumpio2.setText(niño.getName());
                barreraColumpio2.await();
                jTextColumpio2.setText("");
                libreColumpio2=true;
                colaFuera.meter(niño);
                reentrar(niño);
            }
            
            else if(libreColumpio3){
                
                libreColumpio3=false;
                barreraColumpio3.await();
                jTextColumpio3.setText(niño.getName());
                barreraColumpio3.await();
                jTextColumpio3.setText("");
                libreColumpio3=true;
                colaFuera.meter(niño);
                reentrar(niño);

            }
            
            turnoColumpio.release();
            
            
        }
        
        catch(Exception e){}
        
    }
    public void atenderColumpio(Columpio columpio){
        
        monitorPararParque();
        
        try{
            
            //turnoColumpio.acquire();
            
            if(columpio.getName().equals("Columpio1")){                
                barreraColumpio1.await();
                Thread t = (Niño) colaColumpio.sacar();
                colaColumpio1.meter(t);
                System.out.println(t.getName() + " ha entrado en " + columpio.getName());
                Thread.sleep(2000+(int)(1800*Math.random()));
                colaColumpio1.sacar();

                barreraColumpio1.await();
                
            }
            else if(columpio.getName().equals("Columpio2")){
                barreraColumpio2.await();
                Thread t = (Niño) colaColumpio.sacar(); 
                colaColumpio2.meter(t);
                System.out.println(t.getName() + " ha entrado en " + columpio.getName());
                Thread.sleep(2000+(int)(1800*Math.random()));
                colaColumpio2.sacar();

                barreraColumpio2.await();
                
                
            }
            if(columpio.getName().equals("Columpio3")){
                barreraColumpio3.await();
                Thread t = (Niño) colaColumpio.sacar();
                colaColumpio3.meter(t);
                System.out.println(t.getName() + " ha entrado en " + columpio.getName());
                Thread.sleep(2000+(int)(1800*Math.random()));
                colaColumpio3.sacar();
                
                barreraColumpio3.await();
            }
        }
        catch (InterruptedException ex) {
            Logger.getLogger(Parque.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BrokenBarrierException ex) {
            Logger.getLogger(Parque.class.getName()).log(Level.SEVERE, null, ex);
        }
        turnoColumpio.release();
    }
    
    /**
    * Código para tobogan 
    */
    
    public void atenderTobogan(){
        
        monitorPararParque();

        jTextFieldTobogan.setText(" ");
        //monitor();
        //finalizar
        //monitorTobogan();
        try {
            barreraTobogan.await();
            Niño t=(Niño) colaTobogan.sacar();
            tobogan.meter(t);
            Thread.sleep(1500+(int)(1000*Math.random()));
            jTextFieldTobogan.setText(t.getName());
            barreraTobogan.await();
            tobogan.sacar();
            jTextFieldAños.setText("");
            jTextFieldTobogan.setText(""); 
            
        }catch(Exception e){System.out.println(e);}
        
    }
    public void esAtendidoT() {
        //monitor();
        //monitorCarnicero();
        try {
            barreraTobogan.await();
            barreraTobogan.await();            
        } catch (Exception e){System.out.println(e); }
    }
    
    /**
    * Código para tiovivo 
    */
    
    public void esAtendidoTiovivo(){
        try {
            barreraTiovivo.await();
            barreraTiovivo.await();
        } catch (Exception e){ }
    }
    
    public void atenderTiovivo(){
        monitorPararParque();

        jTextFieldTobogan.setText(" ");
        //monitor();
        //finalizar
        //monitorTobogan();
        try {
            barreraTiovivo.await();
            Thread t=(Niño) colaTiovivo.sacar();
            tiovivo.meter(t);
            
            
            barreraTiovivo.await();
            tiovivo.sacar();
            
                  
        }catch(Exception e){}

    }
    public void reentrar(Niño n){
        
        monitorPararParque();

        try {
            Thread.sleep((int) (200+(1800*Math.random())));
        } catch (InterruptedException ex) {
            Logger.getLogger(Parque.class.getName()).log(Level.SEVERE, null, ex);
        }
        colaFuera.sacar(n);
        dentro.meter(n);
        seleccionarAtraccion(n);
        
    }

    public void empezar(Niño niño){
        seleccionarAtraccion(niño);
    }
    
    /**
    * Código para parar el programa
    */
    public synchronized void detener(){
        detenido=true;
        notifyAll();
    }
    public void entrar(Niño niño){
        colaFuera.meter(niño);
        try {
            Thread.sleep((int) (200+(1800*Math.random())));
        } catch (InterruptedException ex) {
            Logger.getLogger(Parque.class.getName()).log(Level.SEVERE, null, ex);
        }
        colaFuera.sacar(niño);
        dentro.meter(niño);
        
    }
    
    
    public synchronized void reanudar(){
        detenido=false;
        notifyAll();
    }
    
    public synchronized void monitorPararParque(){
        while(detenido){
            
            try {
                wait();
            } catch (InterruptedException ex) {
                Logger.getLogger(Parque.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public CyclicBarrier getBarreraTobogan() {
        return barreraTobogan;
    }

    public void setBarreraTobogan(CyclicBarrier barreraTobogan) {
        this.barreraTobogan = barreraTobogan;
    }

    public ListaThreads getColaTobogan() {
        return colaTobogan;
    }

    public void setColaTobogan(ListaThreads colaTobogan) {
        this.colaTobogan = colaTobogan;
    }

    public ListaThreads getColaTiovivo() {
        return colaTiovivo;
    }

    public void setColaTiovivo(ListaThreads colaTiovivo) {
        this.colaTiovivo = colaTiovivo;
    }

    public ListaThreads getColaColumpio() {
        return colaColumpio;
    }

    public void setColaColumpio(ListaThreads colaColumpio) {
        this.colaColumpio = colaColumpio;
    }

    public ListaThreads getColaFuera() {
        return colaFuera;
    }

    public void setColaFuera(ListaThreads colaFuera) {
        this.colaFuera = colaFuera;
    }

    public ListaThreads getDentro() {
        return dentro;
    }

    public void setDentro(ListaThreads dentro) {
        this.dentro = dentro;
    }

    public boolean isDetenido() {
        return detenido;
    }

    public void setDetenido(boolean detenido) {
        this.detenido = detenido;
    }

    public Semaphore getTurnoTobogan() {
        return turnoTobogan;
    }

    public void setTurnoTobogan(Semaphore turnoTobogan) {
        this.turnoTobogan = turnoTobogan;
    }

    public JTextField getjTextFieldColaColumpio() {
        return jTextFieldColaColumpio;
    }

    public void setjTextFieldColaColumpio(JTextField jTextFieldColaColumpio) {
        this.jTextFieldColaColumpio = jTextFieldColaColumpio;
    }

    public Semaphore getTurnoTiovivo() {
        return turnoTiovivo;
    }
    

    public JTextField getjTextFieldColumpio() {
        return jTextFieldColumpio;
    }

    public void setjTextFieldColumpio(JTextField jTextFieldColumpio) {
        this.jTextFieldColumpio = jTextFieldColumpio;
    }

    public JTextField getjTextFieldDecidiendo() {
        return jTextFieldDecidiendo;
    }

    public void setjTextFieldDecidiendo(JTextField jTextFieldDecidiendo) {
        this.jTextFieldDecidiendo = jTextFieldDecidiendo;
    }

    public JTextField getjTextFieldTobogan() {
        return jTextFieldTobogan;
    }

    public void setjTextFieldTobogan(JTextField jTextFieldTobogan) {
        this.jTextFieldTobogan = jTextFieldTobogan;
    }

    public JTextField getjTextFieldColaTiovivo() {
        return jTextFieldColaTiovivo;
    }

    public void setjTextFieldColaTiovivo(JTextField jTextFieldColaTiovivo) {
        this.jTextFieldColaTiovivo = jTextFieldColaTiovivo;
    }

    public JTextField getjTextFieldTiovivo() {
        return jTextFieldTiovivo;
    }

    public void setjTextFieldTiovivo(JTextField jTextFieldTiovivo) {
        this.jTextFieldTiovivo = jTextFieldTiovivo;
    }

    public JTextField getjTextFieldColaTobogan() {
        return jTextFieldColaTobogan;
    }

    public void setjTextFieldColaTobogan(JTextField jTextFieldColaTobogan) {
        this.jTextFieldColaTobogan = jTextFieldColaTobogan;
    }
}
