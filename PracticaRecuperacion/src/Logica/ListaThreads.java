package Logica;
import java.util.*;
import javax.swing.JTextField;

/* 
    La clase ListaThreads permite gestionar las listas de threads en los monitores,
    con métodos para meter y sacar threads en ella. Cada vez que una lista se modifica,
    se imprime su nuevo contenido en el JTextField que toma como parámetro el constructor.
*/

public class ListaThreads
{
    ArrayList<Thread> lista;
    JTextField tf;
    
    public ListaThreads(JTextField tf)
    {
        lista=new ArrayList<Thread>();
        this.tf=tf;
    }
    
    public ListaThreads()
    {
        lista=new ArrayList<Thread>();
        this.tf = null;
    }
    
    public synchronized void meter(Thread t)
    {
        lista.add(t);
        if(tf != null) {
            imprimir();
        }
    }
    
    public synchronized void sacar(Thread t)
    {
        lista.remove(t);
        if(tf != null) {
            imprimir();
        }
    }
    
    public synchronized Thread sacar()
    {
        Thread t = lista.remove(0);
        if(tf != null) {
            imprimir();
        }
        return t;
    }
    
    public synchronized int size() {
        return lista.size();
    }
    
    public synchronized void contar(Thread t){
        int cantidad=lista.size();
        if(tf != null) {
            tf.setText(String.valueOf(cantidad));
        }
    }
    public synchronized void meterE(Thread e){
        lista.add(e);
    }
    public synchronized void sacarE(Thread e){
        lista.remove(e);
    }
    public void imprimir()
    {
        String contenido="";
        for(int i=0; i<lista.size(); i++)
        {
           contenido=contenido+lista.get(i).getName()+" ";
        }
        tf.setText(contenido);
    }
    public synchronized boolean estaVacia(){
        return lista.isEmpty();
    }
}
