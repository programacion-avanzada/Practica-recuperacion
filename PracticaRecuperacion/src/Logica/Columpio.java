/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

/**
 *
 * @author roberto y samuel
 */
public class Columpio extends Thread{
    private Parque parque;

    public Columpio(int id, Parque parque) {
        super.setName("Columpio"+String.valueOf(id));
        this.parque = parque;
    }
    
    public void run(){
        while(true){
            try{
                //System.out.println(this.getName());
                parque.atenderColumpio(this);
            } catch(Exception e){}
        }
        
    }
}
