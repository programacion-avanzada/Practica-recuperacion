/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Distribuida;

import Logica.Parque;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 *
 * @author roberto
 */
public class Tarea extends Thread{
    
    private Socket socket;
    private Parque parque;

    public Tarea(Socket socket, Parque parque) {
        this.socket = socket;
        this.parque = parque;
    }
    
    public void run(){
        
        DataOutputStream salida;
        DataInputStream entrada;
                   
        while(true){
            
            try{
                
                //socket = server.accept();                                   //Esperamos la conexión
                entrada = new DataInputStream(socket.getInputStream());     //Abrimos canales Entrada
                salida = new DataOutputStream(socket.getOutputStream());    //Abrimos canales Salida
                
                String mensaje = entrada.readUTF();                         //Leemos el mensaje del cliente
                System.out.println(mensaje);
                
                if(mensaje.equals("REANUDAR")){    
                    salida.writeUTF(Integer.toString(parque.getColaTobogan().size())+ "$" +Integer.toString(parque.getColaTiovivo().size())+ "$" + Integer.toString(parque.getColaColumpio().size()));
                }
                
                //socket.close();                                             //Se cierra la conexion
            }catch(IOException e){}
        }        
    }

}
