/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Distribuida;

import Logica.Parque;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author roberto
 */
public class ServidorSupervisor extends Thread{
    private ServerSocket server;
    private Parque parque;

    public ServidorSupervisor() {
        try {
            server = new ServerSocket(5000);
        } catch (IOException ex) {
            Logger.getLogger(ServidorSupervisor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public void responde() throws IOException{
        
        Socket socket;
        DataOutputStream salida;
        DataInputStream entrada;    
        Socket socket2 = server.accept();                                               //Escucha para una conexion
        
        Tarea tarea = new Tarea(socket2, parque);
        tarea.start();
    }
    
    public void run() {
        while(true){
            try {
                responde();
            } catch (IOException ex) {
            Logger.getLogger(ServidorSupervisor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }

}
